package br.com.itau.consumergravacsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerGravaCsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerGravaCsvApplication.class, args);
	}

}
