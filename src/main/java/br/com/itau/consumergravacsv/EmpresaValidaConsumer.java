package br.com.itau.consumergravacsv;

import br.com.itau.producercadastra.Empresa;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class EmpresaValidaConsumer {

    @KafkaListener(topics = "spec3-diego-rosendo-3", groupId = "Diego-zoom-mastertech")
    public void receber(@Payload Empresa empresa) {

        //..::[PASSO 01]::.. Receber cadastros de Empresas Válidas da fila
        System.out.println("RECEBI UMA EMPRESA VALIDA (CAPITAL ACIMA DE 1MI) COM CNPJ: " + empresa.getCnpj() + " NOME: " + empresa.getNome());


        //..::[PASSO 02]::.. Gravar em arquivo .CSV
        System.out.println("Grava CSV");
        generateCsvFile("/home/a2/aula-espec3/AtividadeKafkaCadastroEmpresas/cadastroempresasvalidas.csv", empresa);


    }


    private static void generateCsvFile(String sFileName, Empresa empresa) {
        try {
            FileWriter writer = new FileWriter(sFileName, true);

            writer.append('\n');
            writer.append(empresa.getCnpj().toString());
            writer.append(',');
            writer.append(empresa.getNome().toString());


            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
